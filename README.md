# marcels-script

Install node and npm from https://nodejs.org/en/download/.

Download the projec files, extract and using command line navigate to the project directory.

Execute this:

```
$ npm install
```

Once completed, you can use the command

```
$ npm start
```

This will look for files placed at `files` dir.

* `All_Users.txt` - contains all users in HTML format
* `PAID USERS.txt` - contains PAID users as a simple text file (one user per line)

The printed result is the HTML file to paste in the chrome inspector.

