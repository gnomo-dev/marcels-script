var fs = require('fs');
var htmlToJson = require('html-to-json');
var ignoreCase = require('ignore-case');

const usersFile = './files/All_Users.txt';
const paidFile = './files/PAID USERS.txt';
const outPaidFile = './out/users-paid.html';
const outAllFile = './out/users-all.html';

fs.mkdir('out', (err, data) => {
  if (err) {
    if(err.code == 'EEXIST') console.log(`./out dir already exists`);
    else throw err;
  }
  else {
    console.log(`created dir ./out`);
  }
  fs.readFile(usersFile, 'utf8', (err, data) => {
    if (err) throw err
    htmlToJson.parse(data, {
      'users': ['option', {
        'name': function ($option) {
          return $option.text();
        },
        'id': function ($option) {
          return $option.attr('value');
        }
      }]
    }).done(result => {
      fs.readFile(paidFile, 'utf8', (err, data) => {
        let paidUsers = [];
        let lines = data.split('\r\n');
        for (let line of lines) {
          let parts = line.split(/\s/);
          let paidName = parts[0];
          let found = result.users.filter(user => {
            return user.name && ignoreCase.equals(user.name, paidName);
          });
          if(found.length > 0) {
            for (let f of found) {
              paidUsers.push(f);
            }
          }
          else {
              console.log(paidName, 'not found within all users');
          }

        }
        let allUsers = result.users.filter(user => {
          return paidUsers.filter(paidUser => paidUser.id == user.id).length == 0;
        });

        let htmlPaidUsers = '<th valign="top">Exclusive Users<br><select id="chosen" name="chosen[]" multiple="" size="10" style="width: 200px" ondblclick="shuttle(\'chosen\', \'avail\')">';
        for (let paidUser of paidUsers) {
          htmlPaidUsers += `<option value="${paidUser.id}">${paidUser.name}</option>`
        }
        htmlPaidUsers += '</select></th>';
        fs.writeFile(outPaidFile, htmlPaidUsers, (err) => {
          if (err) throw err;
          console.log(`the file ${outPaidFile} has been saved!`);
        });

        let htmlAllUsers = '<th valign="top">Available Users<br><select id="avail" name="avail[]" multiple="" size="10" style="width: 200px" ondblclick="shuttle(\'avail\', \'chosen\')">'
        for (let user of allUsers) {
          htmlAllUsers += `<option value="${user.id}">${user.name}</option>`
        }
        htmlAllUsers += '</select></th>';
        fs.writeFile(outAllFile, htmlAllUsers, (err) => {
          if (err) throw err;
          console.log(`the file ${outAllFile} has been saved!`);
        });
      });
    })
  });
});

